import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import{ NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from './batinmedika/Login';
import Register from './batinmedika/Register';
import Home from './batinmedika/Home';
import Index from './batinmedika/index';
import SplashScreen from './batinmedika/SplashScreen';


export default function App() {
  // const App = () =>{
  return (
    <Index/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
