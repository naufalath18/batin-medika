import React from 'react';
import { 
    View, 
    Text,
    TextInput,
    TouchableOpacity, 
    Platform,
    StyleSheet,
    Image, ImageBackground
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';


export default function App( {navigation} ) {
    return (
      <View style={styles.container}>
          <View style={styles.header}>
                <ImageBackground style={styles.img}
                source={require('./assets/login.jpg')}>
                <Text style={styles.text_header}>Welcome, Please Login</Text>
                </ImageBackground>
          </View>
          <View style={styles.footer}>
              <Text style={styles.text_footer}>Email</Text>
              <View style={styles.action}>
                <FontAwesome
                    name="envelope-o"
                    color="#009387"
                    size={20}
                    >
                </FontAwesome>  
                <TextInput
                    placeholder="Email"
                    style={styles.TextInput}>    
                </TextInput>
                <FontAwesome 
                    name="check"
                    color="green"
                    size={2}>
                </FontAwesome>
                </View>
            <Text style={styles.text_footer, {marginTop:35}}>Password</Text>
                  <View style={styles.action}>
                    <FontAwesome
                        name="lock"
                        color="#009387"
                        size={20}
                        >
                    </FontAwesome>  
                    <TextInput
                        placeholder="Password"
                        secureTextEntry={true}
                        style={styles.TextInput}>    
                    </TextInput>
                    <FontAwesome 
                        name="check"
                        color="green"
                        size={2}>
                    </FontAwesome>
                </View>
                <View>
                    <TouchableOpacity style={styles.button}
                    onPress={ () => navigation.navigate('DrawerStackScreen') } >
                        <Text style={styles.textSign}> Sign In </Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity style={styles.buttonReg}
                    onPress={ () => navigation.navigate('Register') } >
                        <Text style={styles.textReg}> Sign Up </Text>
                    </TouchableOpacity>
                </View>
          </View>
      </View>
    );
};


const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor: '#009387'
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50
    },
    footer: {
        flex: 3,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30
    },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 20,
        marginTop: 230,
        marginLeft: 20,
    },
    text_footer: {
        color: '#05375a',
        fontSize: 18
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    img:{
        marginLeft: -20,
        flex:2,
        width: 400,
        height: 300,
        justifyContent: 'center',
        alignItems: 'stretch'
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 50,
        color: '#05375a',
    },
    button: {
        alignItems: "center",
        backgroundColor: "#009387",
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 30,
        marginTop: 50
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold',
        color: "white"
    },
    buttonReg: {
        alignItems: "center",
        backgroundColor: "white",
        borderColor: "#009387",
        borderWidth: 1,
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 30,
        marginTop: 50
    },
    textReg: {
        fontSize: 18,
        fontWeight: 'bold',
        color: "#009387"
    }
  });