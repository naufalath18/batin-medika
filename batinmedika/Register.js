import React from 'react';
import { 
    View, 
    Text,
    TextInput,
    TouchableOpacity, 
    Platform,
    StyleSheet,
    ImageBackground
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';


export default function App({navigation}) {
    return (
      <View style={styles.container}>
          <View style={styles.header}>
                <ImageBackground style={styles.img}
                source={require('./assets/regis.jpg')}>
                <Text style={styles.text_header}>Register Now !</Text>
                </ImageBackground>
          </View>
          <View style={styles.footer}>
              <Text style={styles.text_footer}>Name</Text>
              <View style={styles.action}>
                <FontAwesome
                    name="user-o"
                    color="#4747d1"
                    size={20}
                    >
                </FontAwesome>  
                <TextInput
                    placeholder="Name"
                    style={styles.TextInput}>    
                </TextInput>
                <FontAwesome 
                    name="check"
                    color="green"
                    size={2}>
                </FontAwesome>
                </View>
            <Text style={styles.text_footer, {marginTop:35}}>Email</Text>
                  <View style={styles.action}>
                    <FontAwesome
                        name="envelope-o"
                        color="#4747d1"
                        size={20}
                        >
                    </FontAwesome>  
                    <TextInput
                        placeholder="Email"
                        secureTextEntry={true}
                        style={styles.TextInput}>    
                    </TextInput>
                </View>
                <Text style={styles.text_footer, {marginTop:35}}>Password</Text>
                  <View style={styles.action}>
                    <FontAwesome
                        name="lock"
                        color="#4747d1"
                        size={20}
                        >
                    </FontAwesome>  
                    <TextInput
                        placeholder="Password"
                        secureTextEntry={true}
                        style={styles.TextInput}>    
                    </TextInput>
                </View>
                <Text style={styles.text_footer, {marginTop:35}}>Confirm Password</Text>
                  <View style={styles.action}>
                    <FontAwesome
                        name="lock"
                        color="#4747d1"
                        size={20}
                        >
                    </FontAwesome>  
                    <TextInput
                        placeholder="Confirm Password"
                        secureTextEntry={true}
                        style={styles.TextInput}>    
                    </TextInput>
                </View>
                <View>
                    <TouchableOpacity style={styles.button}
                    onPress={ () => navigation.navigate('Login') } >
                        <Text style={styles.textSign}> Sign Up </Text>
                    </TouchableOpacity>
                </View>
          </View>
      </View>
    );
};


const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor: '#4747d1'
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50
    },
    footer: {
        flex: 3,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30
    },
    text_header: {
        color: '#4747d1',
        fontWeight: 'bold',
        fontSize: 30,
        marginTop: 200,
        marginLeft: 15
    },
    img:{
        flex:1,
        marginLeft: -20,
        width: 400,
        height: 300,
        justifyContent: 'center',
        alignItems: 'stretch'
      },
    text_footer: {
        color: '#05375a',
        fontSize: 18
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 50,
        color: '#05375a',
    },
    button: {
        alignItems: "center",
        backgroundColor: "#4747d1",
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 30,
        marginTop: 50
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold',
        color: "white"
    }
  });