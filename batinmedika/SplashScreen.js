import React from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    Dimensions,
    StyleSheet,
    StatusBar,
    Image
} from 'react-native';


export default function App() {

    return (
      <View style={styles.container}>
          <View style={styles.header}>
              <Text style={styles.titleHead}>Welcome ... !</Text>

          </View>
          <View style={styles.footer}>
                <Text style={styles.title}>Hallo .. </Text>
                <Text style={styles.text}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</Text>
    
                <View>
                    <TouchableOpacity style={styles.button}
                    onPress={ () => navigation.navigate('RegisterScreen') } >
                    <Text style={styles.textSign}> Sign Up </Text>
                    </TouchableOpacity>
                </View>
          </View>

      </View>
    );
};


const {height} = Dimensions.get("screen");
const height_logo = height * 0.28;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#5b94f0'
  },
  header: {
      flex: 2,
      justifyContent: 'center',
      alignItems: 'center'
  },
  footer: {
      flex: 1,
      backgroundColor: '#fff',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      paddingVertical: 50,
      paddingHorizontal: 30
  },
  logo: {
      width: height_logo,
      height: height_logo
  },
  titleHead: {
      color: '#05375a',
      fontSize: 30,
      fontWeight: 'bold',
      alignItems: 'flex-start'
  },
  title: {
    color: '#05375a',
    fontSize: 30,
    fontWeight: 'bold'
},
  text: {
      color: 'grey',
      marginTop:5
  },
  button: {
      alignItems: 'flex-end',
      marginTop: 30
  },
  button: {
    alignItems: "center",
    backgroundColor: "#5b94f0",
    padding: 10,
    borderRadius: 16,
    marginHorizontal: 30,
    marginTop: 50
},
textSign: {
    fontSize: 18,
    fontWeight: 'bold',
    color: "white"
},
  textSign: {
      color: 'white',
      fontWeight: 'bold'
  }
});