import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Login from './Login';
import Register from './Register';
import Home from './Home';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const DrawerStack = createDrawerNavigator();
const RootStack = createStackNavigator();


// const iconTab = ({ route }) => {
//     return ({
//         tabBarIcon: ({ focused, color, size }) => {
//             if (route.name == 'SkillScreen') {
//                 return <FontAwesome5 name={'user-circle'} size={size} color={color} solid />
//             } else if (route.name == 'ProjectScreen') {
//                 return <FontAwesome5 name="centos" size={size} color={color} solid />
//             } else if (route.name == 'AddScreen') {
//                 return <FontAwesome5 name="calendar-plus" size={size} color={color} solid />
//             }
//         },
//     });
// }



// const TabsStackScreen = () => (
//     <TabsStack.Navigator screenOptions={iconTab} >
//         <TabsStack.Screen name='SkillScreen' component={SkillScreen}
//             options={{
//                 title: 'Skill'
//             }} />
//         <TabsStack.Screen name='ProjectScreen' component={ProjectScreen}
//             options={{
//                 title: 'Proyek'
//             }} />
//         <TabsStack.Screen name='AddScreen' component={AddScreen}
//             options={{
//                 title: 'Plus'
//             }}
//         />
//     </TabsStack.Navigator>
// );

const DrawerStackScreen = () => (
    <DrawerStack.Navigator >
        <DrawerStack.Screen name='Home' component={Home}
            options={{
                title: 'About'
            }}
        />
    </DrawerStack.Navigator>
);

const RootStackScreen = () => (
    <RootStack.Navigator headerMode="none">
        <RootStack.Screen name='Login' component={Login}
            options={{
                title: 'Login'
            }}
        />
        <RootStack.Screen name='Register' component={Register}
            options={{
                title: 'Register'
            }}
        />
        <RootStack.Screen name='DrawerStackScreen' component={DrawerStackScreen}
            options={{
                title: 'Skill'
            }}
        />
    </RootStack.Navigator>
);

export default () => (
    <NavigationContainer>
        <RootStackScreen />
    </NavigationContainer>
);